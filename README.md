# README #

This README will help you to use our TCP / IP implementation.

### Instructions ? ###

* Compte rendu a chaque reunion (lieu, heure, duree)
* Planning previsionnel pour la premiere seance
* Noter tout ce que l'on fait, tout ce que l'on compte faire
* Justifier tous les choix (protocole, language, methode d'analyse)
* Soutenance en Juillet, 45 minutes, chacun presente une partie
* Interface graphique pour suivre les differentes etapes du protocole
  (Connexion, transfert de donnees la plus grosse partie, detection et
  correction des erreurs, controle de flux, temporisation, securite)
* 10 points sur le dossier, 10 points sur l'implementation
* Lancer le protocole avec les etats de l'automate
* Rapport a rendre le jour de la soutenance
* Demonstration sur machine

## Features ##

* Noyau TCP (connexion, transfert de donnees, deconnexion)
* Segmentation
* Securite, authentification, chiffrement
* Checksum
* Donnees urgentes
* Fenetre d'anticipation
* Time out

#### derridjmalik@yahoo.fr ####
