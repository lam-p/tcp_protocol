package network;

import java.lang.Object;
import java.net.Socket;

import network.Trame;

//Worker Server side class
public class Connection implements Runnable
{
  public Connection(Socket client)
  {
    this.client_ = client;
  }

  @Override
  public void run()
  {
    System.out.println("Accepted client. Address : " + this.client_.getInetAddress().getHostName());

  }

  private Socket client_;
}
