package network;

import java.lang.Object;
import java.net.Socket;
import java.util.UUID;

public class Client
{
  public Client(Socket socket_client)
  {
    this.port_server_ = socket_client.getPort();
    this.socket_client_ = socket_client;
    this.id_ = UUID.randomUUID();
  }

  public Client(int port_server)
  {
     this.port_server_ = port_server;
     this.socket_client_ = new Socket();
  }

  public void bind()
  {
  }

  public void connect()
  {
  }

  public void close()
  {
  }

  public UUID getId()
  {
    return this.id_;
  }

  private UUID id_;
  private int port_server_;
  private Socket socket_client_;
}
