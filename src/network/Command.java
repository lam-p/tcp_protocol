package network;

import java.lang.Object;
import java.net.Socket;

import network.Trame;

//Worker Client side class
public class Command implements Runnable
{
  public Command(Socket server)
  {
    this.server_ = server;
  }

  @Override
  public void run()
  {
    System.out.println("Accepted client. Address : " + this.server_.getInetAddress().getHostName());

  }

  private Socket server_;
}
