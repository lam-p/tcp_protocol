package network;

import java.lang.Object;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.IOException;
import java.net.Socket;
import java.net.ServerSocket;

import network.Client;
import network.Connection;

public class Server
{
  public Server(int port)
  {
    this.port_ = port;
    this.socket_server_ = new ServerSocket(port);
    this.connections_ = new ArrayList<Connection>();
  }

  private void accept(Socket socket_client)
  {
    Client client;
    Connection connection;

    client = new Client(socket_client);
    connection = new Connection(client);
    this.connections_.add(connection);
  }

  public void listen() throws IOException
  {
    Socket socket_client;

    socket_client = this.socket_server_.accept();
    accept(socket_client);
  }

  public void answer()
  {
    Iterator<Connection> it = this.connections_.iterator();

    while (it.hasNext())
    {
      //TODO for each connection
      // it = it.next(); Not sure
    }

    while (true)
    {
    }
  }

  public void closeClient(int id)
  {
  }

  public void close()
  {
  }

  private int port_;
  private ServerSocket socket_server_;
  private ArrayList<Connection> connections_;
}
